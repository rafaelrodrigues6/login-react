import React, {Component} from 'react';
import './App.css';

class App extends Component{

  constructor(props){
    super(props);
    
    this.state = {
      email: '',
      senha:'',
      titulo:'',
      //feed:[{id:'1', usuario:'eduardo.lino@pucpr.br', password: '12345'},{id:'2', usuario:'lino@pucpr.br', password: '123'}]
    };

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.mudar = this.mudar.bind(this);
  }
mudar(){
  var novoTitulo = "Acessado com sucesso!";
  var negadoTitulo ='Usuário ou senha incorretos!';
  
  if(this.state.email === 'eduardo.lino@pucpr.br' && this.state.senha === '123456')
    return this.setState({titulo:novoTitulo});
  return this.setState({titulo:negadoTitulo});

}


handleSubmit(event) {
  console.log("Valor recebido " + this.state.email + ' e ' + this.state.senha)

  //this.state.feed.forEach(
    //function(elemento){
      //console.log('Encontrado' + elemento.usuario)
    //}
  //)

  event.preventDefault()
}

handleChange(event){
  this.setState({
    [event.target.name]: event.target.value
  });
  console.log(event.target.value)
}


render(){
  return(
    <div>
      <h1>Login </h1>
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="email" value={this.state.email} onChange={this.handleChange}/>
        <br />
        <input type="password" name="senha" value={this.state.senha} onChange={this.handleChange}/>
        <br />
        <input type="submit" value="Acessar" onClick={this.mudar}/>
        <br />
        <br />
        <label> {this.state.titulo} </label>
      </form>
    </div>
  )
 }

}

export default App;
